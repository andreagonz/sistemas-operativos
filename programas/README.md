# Sistemas Operativos - Tarea 02: Programas

```
Andrea Itzel González Vargas 
```

Para esta tarea se realizaron tres programas:

* shell
* nsca_httpd
* backdoor

## shell

Al correrse el programa ```shell/shell```, se obtiene una *shell* que permite ejecutar comandos:

![Alt](media/s01.png "...")

## nsca_httpd

El programa ```nsca_httpd/tepache``` recibe como argumento el directorio raíz del servidor ```HTTP```, o bien lo obtiene de la variable de entorno ```DocumentRoot```:

![Alt](media/t01.png "...")

Se puede entonces hacer peticiones al servidor. Como respuesta se obtienen los códigos 200, 403, 404 o 500:

![Alt](media/t02.png "...")

![Alt](media/t03.png "...")

![Alt](media/t10.png "...")

También se permite utilizar archivos ```cgi```, por ejemplo el archivo ```nsca_httpd/raiz/hola.cgi```:

```python
#!/usr/bin/python

import cgi, cgitb, os

form = cgi.FieldStorage() 
nombre = cgi.escape(form.getvalue('nombre'))
print "Content-Type: text/html\n"
html = """<html>
<head>
  <title>HOLA</title>
</head>
<body style="color:yellow;background-color:blue;margin-top:50px;">
  <center>
  <h1>Hola %s</h1>
  <img src="https://melbournechapter.net/images/kitten-transparent-white-5.png" width="800px;"/>
  </center>
  <p style="color:white;">Servidor %s</p>
</body>
</html>
"""
software = os.environ["SERVER_SOFTWARE"]
print html % (nombre, software)
```

el cual es ejecutado por el servidor, permitiéndole leer los parámetros de la petición ```GET```:

![Alt](media/t04.png "...")

![Alt](media/t05.png "...")

Si por ejemplo se le quitan los permisos de ejecución a un *script* cgi, se debe esperar recibir un error 500:

![Alt](media/t06.png "...")

![Alt](media/t07.png "...")

En la salida estándar se imprime la bitácora de acceso, en la salida de error estándar la bitácora de errores. 

![Alt](media/t08.png "...")

Para mostrar mensajes de depuración, se puede modificar la constante ```DEBUG``` definida en ```nsca_httpd/tepache.h``` por un valor distinto a 0:

![Alt](media/t09.png "...")

## backdoor

Se debe de correr el archivo ```backdoor/servidor``` para levantar el servidor que estará en escucha de conexiones locales y remotas.

![Alt](media/b01.png "...")

El archivo ```backdoor/remoto``` recibe dos argumentos, el primero es el *host* al cual se conectará, y el segundo es el puerto.

![Alt](media/b02.png "...")

El archivo ```backdoor/local``` crea un cliente que se comunica localmente a través de un *socket* UNIX con el servidor:

![Alt](media/b03.png "...")

Al correr los tres programas se podrá observar que el servidor ha recibido las conexiones:

![Alt](media/b04.png "...")

De esta manera, se puede ejecutar comandos en un equipo remoto:

![Alt](media/b05.png "...")

## pruebas

Para correr las pruebas de los programas basta correr el comando ```make test```:

![Alt](media/p01.png "...")

Para ejecutar las pruebas de ```shell```, es necesario tener instalado [pwntools](https://github.com/Gallopsled/pwntools).
