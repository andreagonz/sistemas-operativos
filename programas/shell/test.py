#!/usr/bin/python
# -*- coding: utf-8 -*-

from pwn import *
import subprocess as sp

def ejecuta(cmd):
    p = sp.Popen(cmd, shell=True, stdin=sp.PIPE, stdout=sp.PIPE)
    out,err = p.communicate()
    return out

total = 0
fail = 0

sh = process('./shell')
sh.sendline('/bin/ls -1\n')
out = ejecuta("ls -1")
l = out.split('\n')
i = 0
r = ''
while 1:
    r = sh.recvline(timeout=2)
    r = r[2:] if r.startswith("$ ") else r
    if not r:
        break
    if l[i].strip() != r.strip():
        fail = 1
    i += 1
total += fail
sh.close()

print "\033[91mFRACASO: Prueba 1\033[0m" if fail > 0 else "\033[92mÉXITO: Prueba 1\033[0m"

fail = 0
sh = process('./shell')
sh.sendline('/usr/bin/id\n')
out = ejecuta("id").strip()
i = 0
r = ''
r = sh.recvline(timeout=2)
r = r[2:] if r.startswith("$ ") else r

if out != r.strip():
    fail = 1
total += fail
sh.close()

print "\033[91mFRACASO: Prueba 2\033[0m" if fail > 0 else "\033[92mÉXITO: Prueba 2\033[0m"

print "\n\033[92m[TODAS LAS PRUEBAS FUERON EXITOSAS]\033[0m" if not total else "\n\033[91m[HUBO ERRORES EN LAS PRUEBAS]\033[0m"
