#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main() {
    while(1) {        
        char ** args = malloc(sizeof(char*));
        int n_elem = 1;
        args[0] = malloc(100);
        memset(args[0], 0, 100);
        printf("$ ");
        int out[2];
        if(pipe(out) < 0) 
            puts("Error al crear pipe");
        else {
            char c;
            int i = 0;
            while((c = getchar()) == ' ');
            while(c != EOF) {
                if(c == '\n')
                    break;
                else if(c == ' ') {
                    i++;
                    c = getchar();
                }
                else {
                    n_elem++;
                    args = realloc(args, sizeof(char*) * (n_elem + 1));
                    args[i] = malloc(100);
                    memset(args[i], 0, 100);
                    int j = 0;
                    while(c != EOF && c != '\n' && c != ' ') {
                        args[i][j] = c;
                        j++;
                        c = getchar();
                    }
                }
            }
            pid_t pid = fork();
            if(pid < 0) 
                puts("Error al crear proceso hijo");
            else {
                if (pid == 0) {
                    dup2(out[1], 1);
                    dup2(out[1], 2);
                    close(out[0]);
                    close(out[1]);
                    execv(args[0], args);
                    exit(0);                        
                } else {
                    close(out[1]);
                    int n;
                    char buf[1024];
                    while((n = read(out[0], buf, sizeof(buf)))) 
                        printf("%.*s", n, buf);
                    wait(NULL);
                }
            }            
        }
        for (int i = 0; i < n_elem; i++)
            free(args[i]);
        free(args);
    }
    return 0;
}
