#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/un.h>
#include <signal.h>
#include "const.h"

void error(char * mensaje, int salir) {
    fprintf(stderr, "ERROR: %s\n", mensaje);
    if(salir)
        exit(salir);
}

void sig(int signum) {
    if(!access(CLIENT_PATH, F_OK))
        unlink(CLIENT_PATH);
    exit(0);
}

int main() {
    int unix_sock;
    struct sockaddr_un addr;
    char out[16000], in[1024];
    
    if((unix_sock = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
        error("socket", 1);

    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, CLIENT_PATH, strlen(CLIENT_PATH));
    if(access(addr.sun_path, F_OK)  ==  0)
        unlink(addr.sun_path);
    
    if(bind(unix_sock, (struct sockaddr *)&addr, sizeof(struct sockaddr_un)) < 0)
        error("No se pudo hacer bind de socket UNIX", 2);

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, SERVER_PATH, strlen(SERVER_PATH));

    if (connect(unix_sock, (struct sockaddr *)&addr, sizeof(addr)) < 0)
        error("connect", 3);

    signal(SIGQUIT, sig);
    signal(SIGINT, sig);
    
    while(1) {
        printf("$ ");
        fflush(stdout);
        int l = read(STDIN_FILENO, in, sizeof(in) - 1);
        in[l] = '\0';
        send(unix_sock, in, l, 0);
        int r;
        r = read(unix_sock, out, sizeof(out) - 1);
        out[r] = '\0';
        printf("%s", out);
    }
    
    close(unix_sock);
}
