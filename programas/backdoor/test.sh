#!/bin/bash

make
total=0
rm -f /tmp/backdoor_prueba*
setsid sh -c './servidor 1>/dev/null' &
pgid=$!
sleep 1
./remoto 127.0.0.1 6666 &
sleep 1
echo ls -1 | ./local > /tmp/backdoor_prueba &
sleep 1
ls -1 > /tmp/backdoor_prueba2
fail=0
while read x <&3 && read y <&4; do
    x=`sed 's/$ \(\)/\1/g'<<< "$x"`
    if ! [[ "$x" == "$y" ]]; then
        echo -e "\033[91mFRACASO: Prueba 1\033[0m"
        fail=1
        break;
    fi;
done 3</tmp/backdoor_prueba 4</tmp/backdoor_prueba2
kill -TERM -$pgid 
if (( $fail == 0 )); then
    echo -e "\033[92mÉXITO: Prueba 1\033[0m"
else
    total=1
fi
rm -f /tmp/backdoor_prueba*

setsid sh -c './servidor 1>/dev/null' &
pgid=$!
sleep 1
./remoto 127.0.0.1 6666 &
sleep 1
echo id | ./local > /tmp/backdoor_prueba &
sleep 1
id > /tmp/backdoor_prueba2
fail=0
while read x <&3 && read y <&4; do
    x=`sed 's/$ \(\)/\1/g'<<< "$x"`
    if ! [[ "$x" == "$y" ]] ; then
        echo -e "\033[91mFRACASO: Prueba 2\033[0m"
        fail=1
        break;        
    fi;
done 3</tmp/backdoor_prueba 4</tmp/backdoor_prueba2
kill -TERM -$pgid
if (( $fail == 0 )); then
    echo -e "\033[92mÉXITO: Prueba 2\033[0m"
else
    total=$(( $total + 1 ))
fi
rm -f /tmp/backdoor_prueba*

echo
if [[ $total == 0 ]]; then
    echo -e "\033[92m[TODAS LAS PRUEBAS FUERON EXITOSAS]\033[0m\n"
else
    echo -e "\033[91m[HUBO ERRORES EN LAS PRUEBAS]\033[0m\n"
fi
