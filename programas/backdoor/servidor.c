#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/un.h>
#include <signal.h>
#include "const.h"

typedef struct {
    int * tcp_sock;
    int * unix_sock;
} socket_struct;

void sig(int signum) {
    if(!access(SERVER_PATH, F_OK))
       unlink(SERVER_PATH);
    exit(0);
}

void error(char * mensaje, int salir) {
    fprintf(stderr, "ERROR: %s\n", mensaje);
    if(salir)
        exit(salir);
}

void * remoto_h(void * socket_s) {
    socket_struct * st = socket_s;
    int tcp_sock = *st->tcp_sock;
    int unix_sock = *st->unix_sock;
    char buf[1024];
    int l = 0;
    while((l = read(tcp_sock, buf, 1024)) > 0)
        send(unix_sock, buf, l, 0);
    close(unix_sock);
    return 0;
}

void * local_h(void * socket_s) {
    socket_struct * st = socket_s;
    int tcp_sock = *st->tcp_sock;
    int unix_sock = *st->unix_sock;
    char buf[1024];
    int l = 0;
    while((l = recv(unix_sock, buf, 1024, 0)) > 0)
        send(tcp_sock, buf, l, 0);
    close(tcp_sock);
    free(st);
    return 0;
}

int main() {
    int tcp_sockfd, unix_sockfd, tcp_sock, unix_sock;
    struct sockaddr_un addr;

    if ((unix_sockfd = socket(AF_UNIX, SOCK_STREAM, 0)) < 0)
        error("socket", 1);

    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, SERVER_PATH, strlen(SERVER_PATH));
    if(access(addr.sun_path, F_OK)  ==  0)
        unlink(addr.sun_path);    
    
    if(bind(unix_sockfd, (struct sockaddr *)&addr, sizeof(struct sockaddr_un)) < 0)
        error("No se pudo hacer bind de socket UNIX", 2);

    if(listen(unix_sockfd, 100) < 0)
        error("No se pudo hacer listen de socket UNIX", 3);
    
    struct sockaddr_in host_addr, client_addr;
    socklen_t sin_size;
    int yes = 1;
    
    if ((tcp_sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        error("Error al crear socket TCP", 3);
    if (setsockopt(tcp_sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
        error("Error al agregar la opcion SO_REUSEADDR en setsockopt", 4);
    
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(PUERTO);
    host_addr.sin_addr.s_addr = INADDR_ANY;
    memset(&(host_addr.sin_zero), '\0', 8);

    if (bind(tcp_sockfd, (struct sockaddr *)&host_addr, sizeof(struct sockaddr)) == -1) 
        error("Error haciendo bind de socket TCP", 5);
    if (listen(tcp_sockfd, 5) == -1)
        error("Error al escuchar por socket TCP", 6);

    sin_size = sizeof(struct sockaddr_in);

    pthread_t thread_id;

    signal(SIGQUIT, sig);
    signal(SIGINT, sig);
    
    while(1) {
        unix_sock = accept(unix_sockfd, NULL, NULL);
        if(unix_sock == -1)
            error("Error al aceptar conexion local", 0);
        puts("Conexion local aceptada");
        tcp_sock = accept(tcp_sockfd, (struct sockaddr *)&client_addr, (socklen_t*)&sin_size);
        char * cliente = inet_ntoa(client_addr.sin_addr);
        if(tcp_sock == -1)
            error("Error al aceptar conexion con cliente", 0);
        else {
            printf("Conexion aceptada desde %s:%d\n", cliente, htons(client_addr.sin_port));
            socket_struct *ss = malloc(sizeof * ss);
            ss->unix_sock = &unix_sock;
            ss->tcp_sock = &tcp_sock;
            if(pthread_create(&thread_id, NULL, remoto_h, ss) < 0)
                fprintf(stderr, "No se pudo crear hilo para cliente remoto %s\n", cliente);
            else if(pthread_create(&thread_id, NULL, local_h, ss) < 0)
                error("No se pudo crear hilo para cliente local\n", 0);
        }
    }
}
