#define BLK "\x1B[90m"
#define RED "\x1B[91m"
#define GRN "\x1B[92m"
#define YEL "\x1B[93m"
#define BLU "\x1B[94m"
#define MAG "\x1B[95m"
#define RES "\x1B[0m"

#define DEBUG 0
#define PUERTO 8080

const char * E403 = "<!doctype html><html lang='es-MX'><head><title>Error 403</title></head><body style='background-color:black;'><center><img src='https://http.cat/403'/></center></body></html>\n";

const char * E404 = "<!doctype html><html lang='es-MX'><head><title>Error 404</title></head><body style='background-color:black;'><center><img src='https://http.cat/404'/></center></body></html>\n";

const char * E500 = "<!doctype html><html lang='es-MX'><head><title>Error 500</title></head><body style='background-color:black;'><center><img src='https://http.cat/500'/></center></body></html>\n";
