#!/bin/bash

make
rm -f /tmp/tepache_prueba*
total=0
fail=0

setsid sh -c './tepache raiz 1>/dev/null 2>/dev/null' &
pgid=$!
sleep 1

curl localhost:8080/index.html 2>/dev/null > /tmp/tepache_prueba1
d=`diff /tmp/tepache_prueba1 raiz/index.html`

if ! [[ -z "$d" ]]; then
    fail=1
fi

if (( $fail == 0 )); then
    echo -e "\033[92mÉXITO: Prueba 1\033[0m"
else
    echo -e "\033[91mFRACASO: Prueba 1\033[0m"
    total=$(( $total + 1 ))
fi

fail=0

export SERVER_SOFTWARE="Tepache v1.0"
export QUERY_STRING="nombre=Chepe"
./raiz/hola.cgi | tail -n +3 > /tmp/tepache_prueba2
curl localhost:8080/hola.cgi?nombre=Chepe 2>/dev/null > /tmp/tepache_prueba3

d=`diff /tmp/tepache_prueba2 /tmp/tepache_prueba3`
if ! [[ -z "$d" ]]; then
    fail=1
fi
if (( $fail == 0 )); then
    echo -e "\033[92mÉXITO: Prueba 2\033[0m"
else
    echo -e "\033[91mFRACASO: Prueba 2\033[0m"
    total=$(( $total + 1 ))
fi

kill -TERM -$pgid

echo
if [[ $total == 0 ]]; then
    echo -e "\033[92m[TODAS LAS PRUEBAS FUERON EXITOSAS]\033[0m"
else
    echo -e "\033[91m[HUBO ERRORES EN LAS PRUEBAS]\033[0m"
fi
rm -f /tmp/tepache_prueba*
