#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <dirent.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <stdarg.h>
#include <pthread.h>
#include <sys/wait.h>
#include "tepache.h"

typedef struct {
    int * socket;
    char * cliente;
} socket_struct;

char * droot;

void pdebug(int count, ...) {
    if(DEBUG) {
        printf(BLK "%s", "DEBUG: ");
        va_list list;
        va_start(list, count); 
        for(int j = 0; j < count; j++)
            printf("%s", va_arg(list, char *));
        va_end(list);
        printf("%s" RES, "\n");
    }
}

void access_log(char * ip, char * recurso, int codigo, int bytes, char * user_agent) {
    char mensaje[2048];
    time_t t = time(NULL);
    struct tm * tm = localtime(&t);
    char fecha[50];
    strftime(fecha, 50, "%d/%b/%Y:%H:%M:%S %z", tm);
    sprintf(mensaje, "%s - - [%s] \"GET %s HTTP/1.1\" %d %d \"-\" \"%s\"\n",
            ip, fecha, recurso, codigo, bytes, user_agent);
    if(codigo == 200)
        printf(GRN  "%s" RES, mensaje);
    else if(codigo == 403)
        printf(MAG  "%s" RES, mensaje);
    else if(codigo == 404) 
        printf(BLU  "%s" RES, mensaje);
    else if(codigo == 500)
        printf(YEL  "%s" RES, mensaje);
}

void error_log(char * cliente, char * mensaje) {
    time_t t = time(NULL);
    struct tm * tm = localtime(&t);
    char fecha[50];
    strftime(fecha, 50, "%c", tm);
    fprintf(stderr, RED "[%s] [core:error] [pid %d:tid %ld] [client %s] %s\n" RES,
            fecha, getpid(), syscall(SYS_gettid), cliente, mensaje);
}

void error(char * mensaje, int salir) {
    fprintf(stderr, RED  "ERROR: %s\n" RES, mensaje);
    if(salir)
        exit(salir);
}

void get_user_agent(char * linea, char * user_agent) {
    strcpy(user_agent, strchr(linea, ':') + 2);
    if(user_agent[strlen(user_agent) - 1] == '\n')
        user_agent[strlen(user_agent) - 1] = '\0';
    if(user_agent[strlen(user_agent) - 1] == '\r')
        user_agent[strlen(user_agent) - 1] = '\0';
}

void get_recurso(char * linea, char * recurso, char * query) {
    char * c = strchr(linea, ' ') + 1;
    if(c) {
        char * r = strchr(c, ' ');
        char * q = strchr(c, '?');
        if(r)
            *r = '\0';
        if(q) {
            *q = '\0';
            strcpy(query, q + 1);
        }
        strcpy(recurso, c);
    }
}

int empieza_con(char *pre, char *str) {
    return strncmp(pre, str, strlen(pre)) == 0;
}

void recibir_mensaje(int socket, char * recurso, char * user_agent, char * query) {
    FILE * fd = fdopen(socket, "rw");
    if(!fd) {
        error("No se pudo leer conexión con cliente", 0);
        return;
    }
    char buf[1050];
    setvbuf(fd, NULL, _IOLBF, 512);
    char * n = fgets(buf, 1048, fd);
    if(!n)
        return;
    get_recurso(buf, recurso, query);
    while(fgets(buf, 512, fd)) {
        if(!strncmp(buf, "\n", 1) || !strncmp(buf, "\r\n", 2))
            break;
        if(empieza_con("User-Agent: ", buf))
            get_user_agent(buf, user_agent);
    }
}

void cat_dir(char * recurso, char * res) {
    char * c = strchr(recurso, '?');
    if(c)
        *c = '\0';
    sprintf(res, "%s%s", droot, recurso);
}

void enviar_cabeceras(int socket, int codigo, int content_lenght) {
    if(codigo == 200)
        send(socket, "HTTP/1.1 200 OK\n", 16, 0);
    else if(codigo == 403)
        send(socket, "HTTP/1.1 403 Forbidden\n", 23, 0);
    else if(codigo == 404)
        send(socket, "HTTP/1.1 404 Not Found\n", 23, 0);
    else
        send(socket, "HTTP/1.1 500 Internal Server Error\n", 35, 0);
    time_t t = time(NULL);
    struct tm * tm = localtime(&t);
    char fecha[50];
    strftime(fecha, 50, "Date: %a, %d %b %Y %H:%H:%M GMT\n", tm);
    send(socket, fecha, strlen(fecha), 0);
    char cl[128];
    sprintf(cl, "Content-Length: %d\n", content_lenght);
    send(socket, cl, strlen(cl), 0);
}

void envia_error(int codigo, int socket, char * cliente, char * recurso,
                 char * user_agent, const char * html, char * mensaje) {
    int len = strlen(html);
    enviar_cabeceras(socket, codigo, len);
    send(socket, "Content-Type: text/html; charset=UTF-8\n\n", 40, 0);
    send(socket, html, len, 0);
    access_log(cliente, recurso, codigo, 0, user_agent);
    if(strlen(mensaje) > 0)
        error_log(cliente, mensaje);    
}

void envia_403(int socket, char * cliente, char * recurso, char * user_agent, char * mensaje) {
    envia_error(403, socket, cliente, recurso, user_agent, E403, mensaje);
}

void envia_404(int socket, char * cliente, char * recurso, char * user_agent) {
    char buf[256];
    sprintf(buf, "Recurso %s no encontrado", recurso);
    envia_error(404, socket, cliente, recurso, user_agent, E404, buf);
}

void envia_500(int socket, char * cliente, char * recurso, char * user_agent, char * mensaje) {
    envia_error(500, socket, cliente, recurso, user_agent, E500, mensaje);
}

int ejecuta(char * cgi, char * cliente, char * user_agent, char * query, char * out) {
    int fd[2];
    if(pipe(fd) < 0)
        return -1;
    pid_t pid = fork();
    if (pid == 0) {
        char buf[8192];
        char ua[256];
        sprintf(ua, "HTTP_USER_AGENT=%s", user_agent);
        char q[256];
        sprintf(q, "QUERY_STRING=%s", query);
        char ip[50];
        sprintf(ip, "REMOTE_ADDRESS=%s", cliente);
        putenv(ua);
        putenv(q);
        putenv(ip);
        putenv("SERVER_SOFTWARE=Tepache v1.0");
        FILE * p = popen(cgi, "r");
        int len;
        if(p) {
            len = fread(buf, 1, 8192, p);
            if(WEXITSTATUS(pclose(p)))
                len = -1;
        } else
            len = -1;
        int * plen = &len;
        close(fd[0]);
        write(fd[1], plen, 4);
        write(fd[1], buf, len);
        exit(0);
    } else {
        wait(NULL);
        close(fd[1]);
        int n;
        int * c = &n;
        if(read(fd[0], c, 4) < 4)
            return -1;
        read(fd[0], out, n);
        return n;
    }
    return -1;
}

void envia_200(int socket, char * cliente, char * archivo,
               char * recurso, char * user_agent, char * query) {
    pdebug(2, "Leyendo archivo ", archivo);
    FILE * fd = fopen(archivo, "rb");
    if(!fd) {
        char buf[256];
        sprintf(buf, "Error al abrir archivo %s", archivo);
        envia_500(socket, cliente, recurso, user_agent, buf);
    } else {        
        char * c = strrchr(archivo, '.');
        if(c && !strncmp(c, ".cgi", 4)) {
            if(!access(archivo, X_OK)) {
                fclose(fd);
                char * out = malloc(8192);
                int len;
                if((len = ejecuta(archivo, cliente, user_agent, query, out)) < 0) {
                    char buf[1024];
                    sprintf(buf, "Error al ejecutar script %s", archivo);
                    envia_500(socket, cliente, recurso, user_agent, buf);
                } else {
                    char * ss = strstr(out, "\n\n");
                    if(ss) {
                        *ss = '\0';
                        char buf[1024];
                        sprintf(buf, "%s\n\n", out);
                        int l = strlen(buf);
                        char * out2 = ss + 2;
                        enviar_cabeceras(socket, 200, len - l);
                        send(socket, buf, l, 0);
                        send(socket, out2, len - l, 0);
                        access_log(cliente, recurso, 200, len - l, user_agent);
                    } else {
                        char buf[1024];
                        sprintf(buf, "Error en script %s, necesita imprimir doble línea", archivo);
                        envia_500(socket, cliente, recurso, user_agent, buf);
                    }
                }
                free(out);
            } else {
                char buf[1024];
                sprintf(buf, "Archivo %s no es ejecutable", archivo);
                envia_500(socket, cliente, recurso, user_agent, buf);
            }
        } else {
            fseek(fd, 0, SEEK_END);
            int tam = ftell(fd);
            fseek(fd, 0, SEEK_SET);        
            enviar_cabeceras(socket, 200, tam);
            send(socket, "Content-Type: text/html; charset=UTF-8\n\n", 40, 0);
            char buf[1024];
            int i = 0;
            int total = 0;
            while((i = fread(buf, 1, 1024, fd))) {
                send(socket, buf, i, 0);
                total += i;
            }
            fclose(fd);
            access_log(cliente, recurso, 200, total, user_agent);
        }       
    }
    pdebug(2, "Datos enviados a ", cliente);
}

void * con_handler(void * socket_struct_p) {
    socket_struct * st = socket_struct_p;
    int socket = *st->socket;
    char recurso[256];
    char user_agent[256];
    char query[256];
    recibir_mensaje(socket, recurso, user_agent, query);
    char archivo[256];
    cat_dir(recurso, archivo);
    if(!access(archivo, F_OK)) {
        struct stat statbuf;
        if(stat(archivo, &statbuf) != 0) {
            char buf[1024];
            sprintf(buf, "Error al verificar archivo %s", archivo);
            envia_500(socket, st->cliente, recurso, user_agent, buf);
        } else {
            if(S_ISREG(statbuf.st_mode)) {
                if(access(archivo, R_OK)) {
                    char mensaje[256];
                    sprintf(mensaje, "No se tienen permisos para leer archivo %s", archivo);
                    envia_403(socket, st->cliente, recurso, user_agent, mensaje);
                } else
                    envia_200(socket, st->cliente, archivo, recurso, user_agent, query);
            } else {
                char mensaje[256];
                sprintf(mensaje, "No se tienen permisos para leer %s", archivo);
                envia_403(socket, st->cliente, recurso, user_agent, mensaje);
            }
        }
    } else
        envia_404(socket, st->cliente, recurso, user_agent);
    close(socket);
    free(st);
    fflush(stdout);
    return 0;
}

void comienza() {
    int sockfd, new_sockfd; 
    struct sockaddr_in host_addr, client_addr;
    socklen_t sin_size;
    int yes = 1;        

    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        error("Error al crear el socket", 3);
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
        error("Error al agregar la opcion SO_REUSEADDR en setsockopt", 4);
    
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(PUERTO);
    host_addr.sin_addr.s_addr = INADDR_ANY;
    memset(&(host_addr.sin_zero), '\0', 8);

    if (bind(sockfd, (struct sockaddr *)&host_addr, sizeof(struct sockaddr)) == -1) 
        error("Error haciendo bind del socket", 5);
    if (listen(sockfd, 5) == -1)
        error("Error al escuchar por el socket", 6);

    sin_size = sizeof(struct sockaddr_in);
    pthread_t thread_id;

    while(1) {
        new_sockfd = accept(sockfd, (struct sockaddr *)&client_addr, (socklen_t*)&sin_size);
        char * cliente = inet_ntoa(client_addr.sin_addr);
        socket_struct *s = malloc(sizeof *s);
        s->socket = &new_sockfd;
        s->cliente = cliente;
        if(new_sockfd == -1)
            error("Error al aceptar conexion con cliente", 0);
        else {
            char buf[10];
            sprintf(buf, "%d", htons(client_addr.sin_port));
            pdebug(4, "Conexion aceptada desde ", cliente, ":", buf);
            if(pthread_create(&thread_id, NULL, con_handler, s) < 0)
                error_log(cliente, "No se pudo crear hilo");
        }
    }     
}

int main(int argc, char * argv[]) {
    if(argc > 1)
        droot = argv[1];
    else
        droot = getenv("DocumentRoot");
    if(!droot) {
        error("ERROR: No se especificó DocumentRoot", 1);
        exit(1);
    }
    DIR* dir = opendir(droot);
    if(!dir) {
        char buf[50];
        sprintf(buf, "ERROR: Directorio %s no existe", droot);
        error(buf, 2);
    }
    closedir(dir);
    printf("Utilizando %s como directorio raíz\n", droot);
    comienza();
    return 0;
}
