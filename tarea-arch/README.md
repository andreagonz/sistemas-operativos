# Andrea Itzel González Vargas

### URL de descripcion de tarea: https://gitlab.com/SistemasOperativos-Ciencias-UNAM/sistemasoperativos-ciencias-unam.gitlab.io/blob/master/tareas/tarea-arch.md

## CPU
```
[user@pc]$ cat /proc/cpuinfo
processor	: 0
vendor_id	: GenuineIntel
cpu family	: 6
model		: 58
model name	: Intel(R) Core(TM) i7-3630QM CPU @ 2.40GHz
stepping	: 9
microcode	: 0x1f
cpu MHz		: 1289.625
cache size	: 6144 KB

processor	: 1
vendor_id	: GenuineIntel
cpu family	: 6
model		: 58
model name	: Intel(R) Core(TM) i7-3630QM CPU @ 2.40GHz
stepping	: 9
microcode	: 0x1f
cpu MHz		: 1256.062
cache size	: 6144 KB

processor	: 2
vendor_id	: GenuineIntel
cpu family	: 6
model		: 58
model name	: Intel(R) Core(TM) i7-3630QM CPU @ 2.40GHz
stepping	: 9
microcode	: 0x1f
cpu MHz		: 1213.031
cache size	: 6144 KB

processor	: 3
vendor_id	: GenuineIntel
cpu family	: 6
model		: 58
model name	: Intel(R) Core(TM) i7-3630QM CPU @ 2.40GHz
stepping	: 9
microcode	: 0x1f
cpu MHz		: 1354.312
cache size	: 6144 KB

processor	: 4
vendor_id	: GenuineIntel
cpu family	: 6
model		: 58
model name	: Intel(R) Core(TM) i7-3630QM CPU @ 2.40GHz
stepping	: 9
microcode	: 0x1f
cpu MHz		: 1392.187
cache size	: 6144 KB

processor	: 5
vendor_id	: GenuineIntel
cpu family	: 6
model		: 58
model name	: Intel(R) Core(TM) i7-3630QM CPU @ 2.40GHz
stepping	: 9
microcode	: 0x1f
cpu MHz		: 1239.281
cache size	: 6144 KB

processor	: 6
vendor_id	: GenuineIntel
cpu family	: 6
model		: 58
model name	: Intel(R) Core(TM) i7-3630QM CPU @ 2.40GHz
stepping	: 9
microcode	: 0x1f
cpu MHz		: 1200.000
cache size	: 6144 KB

processor	: 7
vendor_id	: GenuineIntel
cpu family	: 6
model		: 58
model name	: Intel(R) Core(TM) i7-3630QM CPU @ 2.40GHz
stepping	: 9
microcode	: 0x1f
cpu MHz		: 1202.250
cache size	: 6144 KB
```

## Memoria RAM
```
[user@pc]$ cat /proc/meminfo
MemTotal:        8057736 kB
MemFree:          527312 kB
MemAvailable:    1384976 kB
Buffers:           91200 kB
Cached:          1577440 kB
SwapCached:        32412 kB
Active:          5299032 kB
Inactive:        1660628 kB
Active(anon):    4823140 kB
Inactive(anon):  1314260 kB
Active(file):     475892 kB
Inactive(file):   346368 kB
Unevictable:        3700 kB
Mlocked:            3700 kB
SwapTotal:      12444668 kB
SwapFree:       11675940 kB
Dirty:              1084 kB
```

## Disco Duro
```
[user@pc]$ cat /proc/partitions
major minor  #blocks  name

   8        0  976762584 sda
   8        1    1228800 sda1
   8        2   12444672 sda2
   8        5  963087360 sda5
  11        0    1048575 sr0
   8       16    3192832 sdb
   8       17    3192824 sdb1

[user@pc]$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda      8:0    0 931.5G  0 disk 
├─sda1   8:1    0   1.2G  0 part /boot/efi
├─sda2   8:2    0  11.9G  0 part [SWAP]
└─sda5   8:5    0 918.5G  0 part /
sdb      8:16   1     3G  0 disk 
└─sdb1   8:17   1     3G  0 part /media/chepe/Kindle
sr0     11:0    1  1024M  0 rom
```

## Tarjetas de red

## Dispositivos
