#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/stat.h>
#include <linux/string.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/mutex.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Andrea González <andreagonz@ciencias.unam.mx>");
MODULE_DESCRIPTION("Módulo que en cada lectura regresa el mismo byte.");
MODULE_VERSION("0.1");

#define DEV_NOMBRE "one"
#define CLASE_NOM  "devone"
#define BUF_LON    1024

static DEFINE_MUTEX(do_mutex);

static int  val = 1;
static int  major = 0;
static char buffer[BUF_LON];
static struct class*  clase = NULL;
static struct device* dispositivo = NULL;

static int     dev_open(struct inode *, struct file *);
static int     dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);

module_param(val, int, 0);
MODULE_PARM_DESC(val, "Byte a ser regresado en cada lectura del módulo");

static struct file_operations fops = {
    .owner = THIS_MODULE,
    .write = dev_write,
    .read = dev_read,
    .open = dev_open,
    .release = dev_release,
};

static ssize_t dev_read(struct file *f_ptr,
                        char __user *usr_buffer,
                        size_t cont,
                        loff_t *posicion) {
    int enviado = 0;
    while(enviado < cont) {
        int bytes = cont - enviado <= BUF_LON ? cont - enviado : BUF_LON;
        if(copy_to_user(usr_buffer, buffer, bytes) != 0) {
            printk(KERN_ALERT "devone: Error al enviar %lu caracteres\n", cont);
            return -EFAULT;
        }
        enviado += bytes;
    }
    return enviado;
}

static ssize_t dev_write(struct file *filp, const char *buff, size_t len, loff_t *off) {
   printk(KERN_ALERT "devone: Operacion no soportada\n");
   return -EINVAL;
}

static int dev_open(struct inode *inodep, struct file *filep){
   if(!mutex_trylock(&do_mutex)) {
      printk(KERN_ALERT "devone: Dispositivo está siendo utilizado por otro proceso\n");
      return -EBUSY;
   }
   printk(KERN_INFO "devone: Dispositivo abierto\n");
   return 0;
}

static int dev_release(struct inode *inodep, struct file *filep){
    mutex_unlock(&do_mutex);
    printk(KERN_INFO "devone: Dispositivo cerrado\n");
    return 0;
}

static int __init inicio(void) {
    if((major = register_chrdev(0, DEV_NOMBRE, &fops)) < 0) {
        printk(KERN_ALERT "devone: Error al registrar dispositivo = %d\n", major);
        return major;
    }
    
    clase = class_create(THIS_MODULE, CLASE_NOM);
    if (IS_ERR(clase)) {
        unregister_chrdev(major, DEV_NOMBRE);
        printk(KERN_ALERT "devone: Error al registrar clase de dispositivo\n");
        return PTR_ERR(clase);
    }    
    printk(KERN_INFO "devone: Clase de dispositivo registrada correctamente\n");
    
    dispositivo = device_create(clase, NULL, MKDEV(major, 0), NULL, DEV_NOMBRE);
    if (IS_ERR(dispositivo)) {
        class_destroy(clase);
        unregister_chrdev(major, DEV_NOMBRE);
        printk(KERN_ALERT "devone: Error al crear dispositivo\n");
        return PTR_ERR(dispositivo);
    }
    
    memset(buffer, val, BUF_LON);
    printk(KERN_INFO "devone: Creado dispositivo con valor %d\n", val);
    mutex_init(&do_mutex);
    return 0;
}

static void __exit salida(void) {
    mutex_destroy(&do_mutex);
    if(major != 0) {
        device_destroy(clase, MKDEV(major, 0));
        class_unregister(clase);
        class_destroy(clase);
        unregister_chrdev(major, DEV_NOMBRE);
        printk(KERN_INFO "devone: Dispositivo eliminado\n");
    }
    printk(KERN_INFO "devone: Bye\n");
}

module_init(inicio);
module_exit(salida);
