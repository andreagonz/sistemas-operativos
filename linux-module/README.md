# Sistemas Operativos - Módulos de Kernel

```
Andrea Itzel González Vargas 
```

Para esta tarea se realizó el módulo de kernel ```devone```, el cual crea un dispositivo de caracteres, que al ser leído muestra repetidamente un byte definido.

## Instalación

Para instalar el módulo, primero se debe de copiar las reglas definidas en ```99-devone.rules``` al directorio ```/etc/udev/rules.d```, con el propósito de que sean utilizadas en la creación del dispositivo, y se le asignen permisos de lectura a todos los usuarios, despues se prosigue con la compilación e instalación del módulo:

```
# cp 99-devone.rules /etc/udev/rules.d
# make
# insmod devone.ko
```

Esto creará el dispositivo ```/dev/one```, el cual al ser leído, regresará repetidamente el byte 0x01:

![Alt](media/dev01.png "...")

Para eliminar el módulo se utiliza el siguiente comando:

```
# rmmod devone
```

Si al instalar el módulo se indica el argumento ```val```, se utilizará el valor de éste como valor a ser regresado por el dispositivo:

![Alt](media/dev02.png "...")

Con el comando ```dmesg``` podemos observar los mensajes del kernel:

![Alt](media/dev03.png "...")
